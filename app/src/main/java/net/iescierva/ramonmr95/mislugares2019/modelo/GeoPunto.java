package net.iescierva.ramonmr95.mislugares2019.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import net.iescierva.ramonmr95.mislugares2019.datos.GeoException;

import java.util.Objects;

public class GeoPunto implements Parcelable {

    private double latitud, longitud;

    public GeoPunto(double latitud, double longitud) throws GeoException {
        set(latitud, longitud);
    }

    public GeoPunto(int latitud, int longitud) throws GeoException {
        this(latitud / 1e6, longitud / 1e6);
    }

    public GeoPunto() {
        this.latitud = 0;
        this.longitud = 0;
    }

    public double distancia(GeoPunto punto) {
        final double RADIO_TIERRA = 6371000; // en metros
        double dLat = Math.toRadians(latitud - punto.latitud);
        double dLon = Math.toRadians(longitud - punto.longitud);
        double lat1 = Math.toRadians(punto.latitud);
        double lat2 = Math.toRadians(latitud);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                        Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return c * RADIO_TIERRA;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) throws GeoException {
        if (longitudOK(longitud)) {
            this.longitud = longitud;
        }
        else {
            throw new GeoException("Excepcion: setLongitud(double): ('" +  longitud + "')");
        }

    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) throws GeoException {
        if (latitudOK(latitud)) {
            this.latitud = latitud;
        }
        else {
            throw new GeoException("Excepcion: setLatitud(double): ('" +  latitud + "')");
        }
    }

    public void set(double latitud, double longitud) throws GeoException {
        setLatitud(latitud);
        setLongitud(longitud);
    }

    private boolean latitudOK(double latitud) {
        return latitud >= -90 && latitud <= 90;
    }

    private boolean longitudOK(double longitud) {
        return longitud >= -180 && longitud <= 180;
    }

    /*
    public GeoPunto getSinPosicion() {
        return new GeoPunto();
    }

    public void setSinPosicion() {
        this.latitud = 0;
        this.longitud = 0;
    }
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoPunto)) return false;
        GeoPunto geoPunto = (GeoPunto) o;
        return Double.compare(geoPunto.latitud, latitud) == 0 &&
                Double.compare(geoPunto.longitud, longitud) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitud, longitud);
    }

    @Override
    public String toString() {
        return "GeoPunto{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitud);
        dest.writeDouble(longitud);
    }

    private GeoPunto(Parcel in) {
        latitud = in.readDouble();
        longitud = in.readDouble();
    }

    public static final Parcelable.Creator<GeoPunto> CREATOR = new Parcelable.Creator<GeoPunto>() {
        public GeoPunto createFromParcel(Parcel in) {
            return new GeoPunto(in);
        }

        public GeoPunto[] newArray(int size) {
            return new GeoPunto[size];
        }
    };
}

