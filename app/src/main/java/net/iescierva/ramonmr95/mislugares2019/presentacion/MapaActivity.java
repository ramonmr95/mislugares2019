package net.iescierva.ramonmr95.mislugares2019.presentacion;



import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.iescierva.ramonmr95.mislugares2019.Aplicacion;
import net.iescierva.ramonmr95.mislugares2019.R;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoLugar;
import net.iescierva.ramonmr95.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.GeoException;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresBD;
import net.iescierva.ramonmr95.mislugares2019.modelo.GeoPunto;
import net.iescierva.ramonmr95.mislugares2019.modelo.Lugar;

public class MapaActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private LugaresBD lugares;
    private CasosUsoLugar casosUsoLugar;
    private AdaptadorLugaresBD adaptador;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa);
        lugares = ((Aplicacion) getApplication()).getLugares();
        adaptador = ((Aplicacion) getApplication()).getAdaptador();
        casosUsoLugar = new CasosUsoLugar(this, null, lugares, adaptador);
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.mapa);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            googleMap.setOnInfoWindowClickListener(this);
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
            }
            if (adaptador.getItemCount() > 0) {
                GeoPunto p = null;
                p = adaptador.lugarPosicion(0).getPosicion();
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(p.getLatitud(), p.getLongitud()), 12));
            }
            for (int n = 0; n < adaptador.getItemCount(); n++) {
                Lugar lugar = adaptador.lugarPosicion(n);
                GeoPunto p = lugar.getPosicion();
                if (p != null && p.getLatitud() != 0) {
                    BitmapDrawable iconoDrawable = (BitmapDrawable)
                            ContextCompat.getDrawable(this, lugar.getTipo().getRecurso());
                    Bitmap iGrande = iconoDrawable.getBitmap();
                    Bitmap icono = Bitmap.createScaledBitmap(iGrande,
                            iGrande.getWidth() / 7, iGrande.getHeight() / 7, false);
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(p.getLatitud(), p.getLongitud()))
                            .title(lugar.getNombre()).snippet(lugar.getDireccion())
                            .icon(BitmapDescriptorFactory.fromBitmap(icono)));
                }
            }
        }
        catch (GeoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        try {
            int id = 0;
            boolean encontrado = false;

            while (!encontrado && id < adaptador.getItemCount()) {
                if (adaptador.lugarPosicion(id).getNombre().equals(marker.getTitle())) {
                    casosUsoLugar.mostrar(id);
                    encontrado = true;
                }
                id++;
            }
        } catch (GeoException e) {
            e.printStackTrace();
        }

    }
}
