package net.iescierva.ramonmr95.mislugares2019.presentacion;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;


import net.iescierva.ramonmr95.mislugares2019.R;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoActividades;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoLocalizacion;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoLugar;
import net.iescierva.ramonmr95.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.Lugares;
import net.iescierva.ramonmr95.mislugares2019.Aplicacion;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresBD;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final int SOLICITUD_PERMISO_LOCALIZACION = 1;

    private CasosUsoLugar usoLugar;
    private CasosUsoActividades casosUsoActividades;
    private MediaPlayer mp;
    private CasosUsoLocalizacion usoLocalizacion;
    private static final String TAG = "MisLugares";
    private LugaresBD lugares;
    private AdaptadorLugaresBD adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addToolbarAndFloatButton();
        initDatos();
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
    }

    public void initDatos() {
        lugares = ((Aplicacion) getApplication()).getLugares();
        adaptador = ((Aplicacion) getApplication()).getAdaptador();
        usoLugar = new CasosUsoLugar(this, null, lugares, adaptador);
        casosUsoActividades = new CasosUsoActividades(this);
        usoLocalizacion = new CasosUsoLocalizacion(this, SOLICITUD_PERMISO_LOCALIZACION);
        mp = MediaPlayer.create(this, R.raw.audio);
        mp.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mp.start();
        usoLocalizacion.activar();
        adaptador.notifyDataSetChanged();
    }


    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onPause() {
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
        mp.pause();
        usoLocalizacion.desactivar();
        super.onPause();
    }

    @Override
    protected void onStop() {
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
        mp.pause();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mp.start();
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        mp.stop();
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            casosUsoActividades.lanzarPreferencias(null);
            return true;
        }

        if (id == R.id.acercaDe) {
            casosUsoActividades.lanzarAcercaDe(null);
            return true;
        }

        if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null);
            return true;
        }

        if (id == R.id.menu_mapa) {
            lanzarMapa(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void lanzarVistaLugar(View view) {
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle(R.string.vistalugar_lanzar)
                .setMessage(R.string.vistalugar_lanzar_id)
                .setView(entrada)
                .setPositiveButton(R.string.vistalugar_lanzar_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int id = Integer.parseInt(entrada.getText().toString());
                        usoLugar.mostrar(id);
                    }
                })
                .setNegativeButton(R.string.vistalugar_lanzar_cancelar, null)
                .show();
    }


    public void lanzarMapa(View view) {
        casosUsoActividades.lanzarMapa(null);
    }


    public void cerrar(View view) {
        finish();
    }


    public void addToolbarAndFloatButton() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                usoLugar.nuevo();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle estadoGuardado) {
        super.onSaveInstanceState(estadoGuardado);
        if (mp != null) {
            int pos = mp.getCurrentPosition();
            estadoGuardado.putInt("posicion", pos);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle estadoGuardado) {
        super.onRestoreInstanceState(estadoGuardado);
        if (estadoGuardado != null && mp != null) {
            int pos = estadoGuardado.getInt("posicion");
            mp.seekTo(pos);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SOLICITUD_PERMISO_LOCALIZACION
                && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            usoLocalizacion.permisoConcedido();
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(CasosUsoLocalizacion.TAG, "Nueva localización: " + location);
        usoLocalizacion.actualizaMejorLocaliz(location);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(CasosUsoLocalizacion.TAG, "Cambia estado: " + provider);
        usoLocalizacion.activarProveedores();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(CasosUsoLocalizacion.TAG, "Se habilita: " + provider);
        usoLocalizacion.activarProveedores();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(CasosUsoLocalizacion.TAG, "Se deshabilita: " + provider);
        usoLocalizacion.activarProveedores();
    }

    public Lugares getLugares() {
        return lugares;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CasosUsoActividades.RESULTADO_PREFERENCIAS) {
            adaptador.setCursor(lugares.extraeCursor());
            adaptador.notifyDataSetChanged();
            if (usoLugar.obtenerFragmentVista() != null)
                usoLugar.mostrar(0);
        }
    }
}
