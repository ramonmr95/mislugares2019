package net.iescierva.ramonmr95.mislugares2019.datos;

import android.database.Cursor;

import net.iescierva.ramonmr95.mislugares2019.modelo.Lugar;
import net.iescierva.ramonmr95.mislugares2019.presentacion.AdaptadorLugares;

public class AdaptadorLugaresBD extends AdaptadorLugares {

    protected Cursor cursor;

    public AdaptadorLugaresBD(Lugares lugares, Cursor cursor) {
        super(lugares);
        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    public Lugar lugarPosicion(int posicion) throws GeoException {
        cursor.moveToPosition(posicion);
        return LugaresBD.extraeLugar(cursor);
    }

    public int idPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        if (cursor.getCount() > 0)
            return cursor.getInt(0);
        else
            return -1;
    }

    public int posicionId(int id) {
        int pos = 0;
        while (pos < getItemCount() && idPosicion(pos) != id)
            pos++;
        return pos > getItemCount() ? -1 : pos;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        try {
            Lugar lugar = lugarPosicion(posicion);
            holder.personaliza(lugar);
            holder.itemView.setTag(posicion);
        } catch (GeoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

}
