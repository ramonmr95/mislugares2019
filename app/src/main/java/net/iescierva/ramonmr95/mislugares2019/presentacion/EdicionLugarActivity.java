package net.iescierva.ramonmr95.mislugares2019.presentacion;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.ramonmr95.mislugares2019.Aplicacion;
import net.iescierva.ramonmr95.mislugares2019.R;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoLugar;
import net.iescierva.ramonmr95.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.GeoException;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresBD;
import net.iescierva.ramonmr95.mislugares2019.modelo.Lugar;
import net.iescierva.ramonmr95.mislugares2019.modelo.TipoLugar;


public class EdicionLugarActivity extends AppCompatActivity {

    private LugaresBD lugares;
    private AdaptadorLugaresBD adaptador;
    private CasosUsoLugar usoLugar;
    private int pos;
    private Lugar lugar;
    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText comentario;
    private int _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.edicion_lugar);
            adaptador = ((Aplicacion) getApplication()).getAdaptador();
            lugares = ((Aplicacion) getApplication()).getLugares();

            Bundle extras = getIntent().getExtras();
            pos = extras.getInt("pos", -1);
            _id = extras.getInt("_id", -1);
            if (_id != -1)
                lugar = lugares.elemento(_id);
            else
                lugar = adaptador.lugarPosicion(pos);

            usoLugar = new CasosUsoLugar(this, null, lugares, adaptador);

            addSpinner();
            actualizaVistas();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edicion_lugar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (_id == -1)
            _id = adaptador.idPosicion(pos);

        switch (item.getItemId()) {
            case R.id.cancelEditButton:
                if (_id != -1) lugares.borrar(_id);
                finish();
                return true;
            case R.id.AcceptEditButton:
                if (_id == -1)
                    _id = adaptador.idPosicion(pos);
                guardarEdit();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addSpinner() {
        tipo = findViewById(R.id.tipo);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());
        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        tipo.setAdapter(adaptador);
        tipo.setSelection(lugar.getTipo().ordinal());
    }

    public void actualizaVistas() {
        nombre = findViewById(R.id.NombreEditText);
        nombre.setText(lugar.getNombre());

        direccion = findViewById(R.id.direccionEditText);
        direccion.setText(lugar.getDireccion());

        telefono = findViewById(R.id.telefonoEditText);
        telefono.setText(String.valueOf(lugar.getTelefono()));

        url = findViewById(R.id.urlEditText);
        url.setText(lugar.getUrl());

        comentario = findViewById(R.id.comentariosEditText);
        comentario.setText(lugar.getComentario());
    }

    public void guardarEdit() {
        lugar.setNombre(nombre.getText().toString());
        lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
        lugar.setDireccion(direccion.getText().toString());
        lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
        lugar.setUrl(url.getText().toString());
        lugar.setComentario(comentario.getText().toString());
        usoLugar.guardar(_id, lugar);
        finish();
    }
}
