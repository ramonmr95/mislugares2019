package net.iescierva.ramonmr95.mislugares2019;

import android.app.Application;

import net.iescierva.ramonmr95.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.Lugares;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresLista;
import net.iescierva.ramonmr95.mislugares2019.modelo.GeoPunto;
import net.iescierva.ramonmr95.mislugares2019.presentacion.AdaptadorLugares;

public class Aplicacion extends Application {

    private LugaresBD lugares;
    private AdaptadorLugaresBD adaptador;
    public GeoPunto posicionActual = new GeoPunto();

    @Override public void onCreate() {
        super.onCreate();
        lugares = new LugaresBD(this);
        adaptador= new AdaptadorLugaresBD(lugares, lugares.extraeCursor());
    }

    public LugaresBD getLugares() {
        return lugares;
    }

    public AdaptadorLugaresBD getAdaptador() {
        return adaptador;
    }
}