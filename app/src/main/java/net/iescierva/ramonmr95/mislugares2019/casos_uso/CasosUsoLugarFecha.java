package net.iescierva.ramonmr95.mislugares2019.casos_uso;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import net.iescierva.ramonmr95.mislugares2019.R;
import net.iescierva.ramonmr95.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.GeoException;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresBD;
import net.iescierva.ramonmr95.mislugares2019.modelo.Lugar;
import net.iescierva.ramonmr95.mislugares2019.presentacion.DialogoSelectorFecha;
import net.iescierva.ramonmr95.mislugares2019.presentacion.DialogoSelectorHora;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class CasosUsoLugarFecha extends CasosUsoLugar implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    int pos = -1;
    Lugar lugar;

    public CasosUsoLugarFecha(FragmentActivity actividad, Fragment fragment, LugaresBD lugares, AdaptadorLugaresBD adaptador) {
        super(actividad, fragment, lugares, adaptador);
    }

    public void cambiarHora(int pos) {
        try {
            lugar = adaptador.lugarPosicion(pos);
        } catch (GeoException e) {
            e.printStackTrace();
        }
        this.pos = pos;
        DialogoSelectorHora dialogo = new DialogoSelectorHora();
        dialogo.setOnTimeSetListener(this);
        Bundle args = new Bundle();
        args.putLong("fecha", lugar.getFecha());
        dialogo.setArguments(args);
        dialogo.show(actividad.getSupportFragmentManager(), "selectorHora");
    }

    @Override
    public void onTimeSet(TimePicker vista, int hora, int minuto) {
        Calendar calendario = Calendar.getInstance();
        calendario.setTimeInMillis(lugar.getFecha());
        calendario.set(Calendar.HOUR_OF_DAY, hora);
        calendario.set(Calendar.MINUTE, minuto);
        lugar.setFecha(calendario.getTimeInMillis());
        actualizaPosLugar(pos, lugar);
        TextView textView = actividad.findViewById(R.id.hora);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(DateFormat.getTimeInstance().format(
                    new Date(lugar.getFecha())));
        }

    }

    public void cambiarFecha(int pos) {
        try {
            lugar = adaptador.lugarPosicion(pos);
        } catch (GeoException e) {
        }
        this.pos = pos;
        DialogoSelectorFecha dialogo = new DialogoSelectorFecha();
        dialogo.setOnDateSetListener(this);
        Bundle args = new Bundle();
        args.putLong("fecha", lugar.getFecha());
        dialogo.setArguments(args);
        dialogo.show(actividad.getSupportFragmentManager(), "selectorFecha");
    }

    @Override
    public void onDateSet(DatePicker view, int anno, int mes, int dia) {
        Calendar calendario = Calendar.getInstance();
        calendario.setTimeInMillis(lugar.getFecha());
        calendario.set(Calendar.YEAR, anno);
        calendario.set(Calendar.MONTH, mes);
        calendario.set(Calendar.DAY_OF_MONTH, dia);
        lugar.setFecha(calendario.getTimeInMillis());
        actualizaPosLugar(pos, lugar);
        TextView textView = actividad.findViewById(R.id.fecha);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(DateFormat.getDateInstance().format(
                    new Date(lugar.getFecha())));
        }

    }
}

