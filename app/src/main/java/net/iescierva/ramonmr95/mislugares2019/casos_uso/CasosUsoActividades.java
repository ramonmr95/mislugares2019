package net.iescierva.ramonmr95.mislugares2019.casos_uso;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import androidx.fragment.app.FragmentActivity;

import net.iescierva.ramonmr95.mislugares2019.presentacion.AcercaDeActivity;
import net.iescierva.ramonmr95.mislugares2019.presentacion.MapaActivity;
import net.iescierva.ramonmr95.mislugares2019.presentacion.PreferenciasActivity;
import net.iescierva.ramonmr95.mislugares2019.presentacion.VistaLugarActivity;

public class CasosUsoActividades {

    public static final int RESULTADO_PREFERENCIAS = 0;

    private FragmentActivity actividad;

    public CasosUsoActividades(FragmentActivity actividad) {
        this.actividad = actividad;
    }

    public void lanzarAcercaDe(View view){
        Intent i = new Intent(actividad, AcercaDeActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarPreferencias(View view){
        Intent i = new Intent(actividad, PreferenciasActivity.class);
        actividad.startActivityForResult(i, RESULTADO_PREFERENCIAS);
    }

    public void lanzarVistaLugarActividad(View view) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarMapa(View view) {
        Intent intent = new Intent(actividad, MapaActivity.class);
        actividad.startActivity(intent);
    }

}
