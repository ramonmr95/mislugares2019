package net.iescierva.ramonmr95.mislugares2019.presentacion;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import net.iescierva.ramonmr95.mislugares2019.Aplicacion;
import net.iescierva.ramonmr95.mislugares2019.R;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoLugar;
import net.iescierva.ramonmr95.mislugares2019.casos_uso.CasosUsoLugarFecha;
import net.iescierva.ramonmr95.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.ramonmr95.mislugares2019.datos.GeoException;
import net.iescierva.ramonmr95.mislugares2019.datos.LugaresBD;
import net.iescierva.ramonmr95.mislugares2019.modelo.Lugar;

import java.text.DateFormat;
import java.util.Date;

public class VistaLugarFragment extends Fragment {

    private final static int RESULTADO_EDITAR = 1;
    private final static int RESULTADO_GALERIA = 2;
    private final static int RESULTADO_FOTO = 3;
    private static final int MY_PERMISSIONS_READ_EXTERNAL_STORAGE = 4;
    private static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 5;

    private Uri uriUltimaFoto;
    private ImageView foto;

    private LugaresBD lugares;
    private AdaptadorLugaresBD adaptador;

    private CasosUsoLugar usoLugar;
    private CasosUsoLugarFecha usoLugarFecha;
    public int pos;
    private Lugar lugar;

    public int _id;

    private View v;

    @Override
    public View onCreateView(LayoutInflater inflador, ViewGroup contenedor, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View vista = inflador.inflate(R.layout.vista_lugar, contenedor, false);
        return vista;
    }

    //recogida de parámetros e inicialización
    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null)
            pos = extras.getInt("pos", 0);
        else
            pos = 0;

        v = getView();

        addOnClickListeners();
        initialize();
        update();
    }

    private void addOnClickListeners() {
        v.findViewById(R.id.url).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.verPgWeb(lugar);
            }
        });

        v.findViewById(R.id.telefono).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.llamarTelefono(lugar);
            }
        });

        v.findViewById(R.id.direccion).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.verMapa(lugar);
            }
        });

        v.findViewById(R.id.vista_lugar).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.verMapa(lugar);
            }
        });

        v.findViewById(R.id.galeria).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.galeria(RESULTADO_GALERIA);
            }
        });

        v.findViewById(R.id.cerrar).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                usoLugar.ponerFoto(pos, "", foto);
            }
        });

        v.findViewById(R.id.camara).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
               uriUltimaFoto = usoLugar.tomarFoto(RESULTADO_FOTO);
            }
        });

        v.findViewById(R.id.hora).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoLugarFecha.cambiarHora(pos);
            }
        });

        v.findViewById(R.id.fecha).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoLugarFecha.cambiarFecha(pos);
            }
        });

    }

    private void initialize() {
        adaptador = ((Aplicacion) getActivity().getApplication()).getAdaptador();
        lugares = ((Aplicacion) getActivity().getApplication()).getLugares();
        usoLugarFecha = new CasosUsoLugarFecha(getActivity(), this, lugares, adaptador);
        usoLugar = new CasosUsoLugar(getActivity(), this, lugares, adaptador);

        try {
            lugar = adaptador.lugarPosicion(pos);
        } catch (GeoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.vista_lugar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_compartir:
                usoLugar.compartir(lugar);
                return true;
            case R.id.accion_llegar:
                usoLugar.verMapa(lugar);
                return true;
            case R.id.accion_editar:
                Intent intent = new Intent(getActivity(), EdicionLugarActivity.class);
                intent.putExtra("_id", _id);
                intent.putExtra("pos", pos);
                intent.putExtra("lugar", lugar);
                startActivityForResult(intent, RESULTADO_EDITAR);
                return true;
            case R.id.accion_borrar:
                usoLugar.borrar(_id);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void verMapa(View view) {
        usoLugar.verMapa(lugar);
    }

    public void llamarTelefono(View view) {
        usoLugar.llamarTelefono(lugar);
    }

    public void ponerDeGaleria(View view) {
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            usoLugar.galeria(RESULTADO_GALERIA);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_READ_EXTERNAL_STORAGE);
        }
    }

    public void tomarFoto(View view) {
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            uriUltimaFoto = usoLugar.tomarFoto(RESULTADO_FOTO);
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        }
    }

    public void eliminarFoto(View view) {
        usoLugar.ponerFoto(pos, "", foto);
    }

    public void update() {
        if (adaptador.getItemCount() == 0)
            return;

        _id = adaptador.idPosicion(pos);

        foto = v.findViewById(R.id.foto);
        usoLugar.visualizarFoto(lugar, foto);

        TextView nombre = v.findViewById(R.id.nombre);
        nombre.setText(lugar.getNombre());
        TextView tipo = v.findViewById(R.id.tipo);
        tipo.setText(lugar.getTipo().getTexto());

        if (lugar.getDireccion().isEmpty()) {
            v.findViewById(R.id.direccion).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.direccion).setVisibility(View.VISIBLE);
            TextView direccion = v.findViewById(R.id.direccion);
            direccion.setText(lugar.getDireccion());
        }

        if (lugar.getTelefono() == 0) {
            v.findViewById(R.id.telefono).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.telefono).setVisibility(View.VISIBLE);
            TextView telefono = v.findViewById(R.id.telefono);
            telefono.setText(String.valueOf(lugar.getTelefono()));
        }

        if (lugar.getUrl().isEmpty()) {
            v.findViewById(R.id.url).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.url).setVisibility(View.VISIBLE);
            TextView url = v.findViewById(R.id.url);
            url.setText(lugar.getUrl());
        }

        if (lugar.getComentario().isEmpty()) {
            v.findViewById(R.id.comentario).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.url).setVisibility(View.VISIBLE);
            TextView comentario = v.findViewById(R.id.comentario);
            comentario.setText(lugar.getComentario());
        }

        TextView fecha = v.findViewById(R.id.fecha);
        fecha.setText(DateFormat.getDateInstance().format(
                new Date(lugar.getFecha())));

        TextView hora = v.findViewById(R.id.hora);
        hora.setText(DateFormat.getTimeInstance().format(
                new Date(lugar.getFecha())));

        RatingBar valoracion = v.findViewById(R.id.valoracion);
        valoracion.setRating(lugar.getValoracion());
        valoracion.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar,
                                                float valor, boolean fromUser) {
                        lugar.setValoracion(valor);
                        usoLugar.actualizaPosLugar(pos, lugar);
                        pos = adaptador.posicionId(_id);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULTADO_EDITAR) {
            try {
                lugar = lugares.elemento(_id);
                pos = adaptador.posicionId(_id);
                update();
                v.findViewById(R.id.scrollView1).invalidate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == RESULTADO_GALERIA) {
            if (resultCode == Activity.RESULT_OK) {
                usoLugar.ponerFoto(pos, data.getDataString(), foto);
            } else {
                Toast.makeText(getActivity(), "Foto no cargada", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == RESULTADO_FOTO) {
            if (resultCode == Activity.RESULT_OK && uriUltimaFoto != null) {
                lugar.setFoto(uriUltimaFoto.toString());
                usoLugar.ponerFoto(pos, lugar.getFoto(), foto);
            } else {
                Toast.makeText(getActivity(), "Error en captura", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE) {
            if (resultCode == Activity.RESULT_OK && uriUltimaFoto != null) {
                tomarFoto(null);
            }
        } else if (requestCode == MY_PERMISSIONS_READ_EXTERNAL_STORAGE) {
            if (resultCode == Activity.RESULT_OK) {
                ponerDeGaleria(null);
            }
        }

    }

}


